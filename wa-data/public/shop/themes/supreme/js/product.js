/* supreme 1.4.8.4 */
function Product(form, options) {
	this.form = $(form);

	this.wrapper = this.form.parents('.product_page');
	this.coreImage = this.wrapper.find('.product-core-image');
	this.video = this.wrapper.find('.product-gallery-wrap .video-container');
	this.moreImages = this.wrapper.find('.more-images');
	this.productImage = this.wrapper.find('.product-image');
	this.badgeWrapper = $('.product-core-image .badge-wrapper');
	this.add2cart = this.form.find(".add2cart");
	this.prices = this.add2cart.find('.prices');
	this.button = this.add2cart.find("button[type=submit]");
	this.buttonStorequickorder = this.form.find(".storequickorder-button"); // easy IT
	this.buttonQuickorder = this.form.find(".quickorder-custom-button"); // Igor Gaponov
	this.isQuickView = this.form.closest('.remodal').length;
	this.remodal = this.form.closest('.remodal');

	for (var k in options) {
		this[k] = options[k];
	}
	var self = this;

	if (self.isQuickView) {
		var remodalOpenTimerId = setTimeout(function tick() {
			if(self.remodal.hasClass('remodal-is-opened')){
				self.moreImagesSlider();
			}
			else{
				remodalOpenTimerId = setTimeout(tick, 50);
			}
		}, 50);

		$(document).on('closed', '.remodal', function() {
			self.moreImagesSliderDestroy();
		});
	}
	else {
		self.moreImagesSlider();
	}

	this.form.find('.pp_tabpanel .nav-tabs li:first').addClass('active');
	this.form.find('.pp_tabpanel .tab-content .tab-pane:first').addClass('active');


	if (window.location.hash == '#pp_specs') {
		window.location.hash = '';
		self.scrollToTab('#pp_specs');
	}
	$('.maincontent .link_to_features').on('click', function() {
		self.scrollToTab('#pp_specs');
		return false;
	});

	if($('.tab-pane .features tr').length > $('.pp_short_features_table tr:visible').length){
		$('.maincontent .link_to_features').show();
	}


	if (window.location.hash == '#pp_reviews') {
		window.location.hash = '';
		self.scrollToTab('#pp_reviews');
	}
	$('.maincontent .link_to_reviews').on('click', function() {
		self.scrollToTab('#pp_reviews');
		return false;
	});


	// moreImages on click - imageChange
	self.moreImages.find('.image:not(.video) a').on('click', function(e) {
		e.preventDefault();
		var imgId = $(this).attr('data-img-id');
		var sku_radios = self.form.find('.skus');
		var sku_features = self.form.find('.inline-select');
		self.imageChange(imgId);
		if(sku_radios.length && self.productImgSkuIds && self.productImgSkuIds[imgId]){
			sku_radios.find('input[value='+self.productImgSkuIds[imgId]+']').click();
			self.updatePrice();
		}
		return false;
	});

	// product image video
	self.moreImages.find('.image.video a').on('click', function() {
		self.coreImage.hide();
		self.video.show();
		self.moreImages.find('.image').removeClass('selected');
		$(this).parent().addClass('selected');
		return false;
	});

	self.imagePopup();

	// add to cart block: services
	this.form.find(".services input[type=checkbox]").on('click', function() {
		var obj = $('select[name="service_variant[' + $(this).val() + ']"]');
		if (obj.length) {
			if ($(this).is(':checked')) {
				obj.removeAttr('disabled');
			}
			else {
				obj.attr('disabled', 'disabled');
			}
		}
		self.cartButtonVisibility(true);
		self.updatePrice();
	});

	this.form.find(".services .service-variants").on('change', function() {
		self.cartButtonVisibility(true);
		self.updatePrice();
	});

	this.form.find('.inline-select a').on('click', function() {
		var d = $(this).closest('.inline-select');
		d.find('a.selected').removeClass('selected');
		$(this).addClass('selected');
		d.find('.sku-feature').val($(this).data('value')).change();
		return false;
	});

	this.form.find(".skus input[type=radio]").on('click', function() {
		if (!$(".product-image-" + $(this).data('image-id')).parent().hasClass('selected')) {
			var imgId = $(this).data('image-id');
			if(imgId){
				self.imageChange(imgId);
			}
		}
		if ($(this).data('disabled')) {
			self.button.attr('disabled', 'disabled');
			self.b1cPluginButtons('hide');
		}
		else {
			self.button.removeAttr('disabled');
			self.b1cPluginButtons('show');
		}
		var sku_id = $(this).val();
		self.updateSkuServices(sku_id);
		self.cartButtonVisibility(true);
		self.updatePrice();
	});

	if(this.form.find('.skus input[type=radio]').length && !this.form.find('.skus input[type=radio]:not(:disabled)').length){
		this.form.find(".stocks").css('visibility', 'visible');
		this.form.find(".unavailable-stock").show().siblings().hide();

		self.b1cPluginButtons('hide');
	}

	var $initial_cb = this.form.find(".skus input[type=radio]:checked:not(:disabled)");
	if (!$initial_cb.length) {
		$initial_cb = this.form.find(".skus input[type=radio]:not(:disabled):first").prop('checked', true);
	}
	$initial_cb.click();

	// features - select
	this.form.find("select.sku-feature").on('change', function() {
		var key = "";
		var currentFeaturesSelected = {};

		self.form.find(".sku-feature").each(function() {
			key += $(this).data('feature-id') + ':' + $(this).val() + ';';
			currentFeaturesSelected[$(this).data('feature-id')] = $(this).val();
		});

		var sku = self.features[key];

		if (typeof $.hideskusPlugin !== 'undefined') {
			if (!$.hideskusPlugin.start(self, key)) {
				return false;
			}
		}

		self.setFeature(sku);
	});

	// features - inline buttons
	this.form.find(".inline-select .sku-feature").on('change', function() {
		var selfFeature = $(this);
		var thisFeatureKey = $(this).attr('data-feature-id');
		var thisFeatureVal = $(this).val();
		var key = "";
		var currentFeaturesSelected = {};

		self.form.find(".sku-feature").each(function() {
			key += $(this).data('feature-id') + ':' + $(this).val() + ';';
			currentFeaturesSelected[$(this).data('feature-id')] = $(this).val();
		});

		var sku = self.features[key];

		if (typeof $.hideskusPlugin !== 'undefined') {
			if (!$.hideskusPlugin.start(self, key)) {
				return false;
			}
		}

		if(self.advancedParameterSelection){
			if(self.form.find(".sku-feature").length < 3 && self.productAvailable){

				// if CAN buy sku, then use this sku and
				if(sku.available){

					self.form.find('.options .disabled').removeClass('disabled');
					$.each(self.features,function(key, value) {
						var res = key.match(thisFeatureKey+':'+thisFeatureVal+';');
						if(res && !value.available){
							var arr = key.split(';');

							$.each(arr, function(key, value){
								if(value.length !== 0 ){
									var el = value.split(':');

									if(!(currentFeaturesSelected[el[0]] != null && currentFeaturesSelected[el[0]] == el[1])){
										self.form.find('.p-feature-select-' + el[0] + ' .p-feature-' + el[1]).addClass('disabled');
									}
								}
							});

						}
					});
				}
				// if CANNOT buy sku, then set first available and recall this function by "selfFeature.change();"
				else{

					var firstAvailableSKU = false;
					for(var key in self.features){
						if(key.match(thisFeatureKey+':'+thisFeatureVal+';') && self.features[key].available){
							// self.form.find('.options .disabled').removeClass('disabled');
							sku = self.features[key];
							firstAvailableSKU = key.split(';');

							break;
						}

					}

					if(firstAvailableSKU){

						self.form.find('.inline-select a').removeClass('selected');

						$.each(firstAvailableSKU, function(key, value){
							if(value.length !== 0 ){
								var el = value.split(':');
								self.form.find('input[name="features['+ el[0] +']"]').val(el[1]);
								self.form.find('.p-feature-select-'+ el[0] +' .p-feature-' + el[1]).addClass('selected');
							}
						});
						selfFeature.change();
					}
				}
			}
		}
		self.setFeature(sku);
	});

	this.form.find(".sku-feature:first").change();


	this.form.on('submit', function() {
		var f = $(this);
		f.find('.adding2cart').addClass('icon24 loading').show();

		$.post(f.attr('action') + '?html=1', f.serialize(), function(response) {
			f.find('.adding2cart').hide();

			if (response.status == 'ok') {
				if ($('.cart-summary-page').length == 1) {
					window.location.reload();
				}

				var cart_total = $(".cart_total");
				var cart_count = $(".cart_count");
				cart_total.closest('#cart').removeClass('empty');

				if (self.isQuickView) {
					$('[data-remodal-id=remodal_quick_view]').trigger("quick_view_added_to_basket");
				}

				if (MatchMedia("only screen and (max-width: 767px)")) {
					f.find('span.added2cart').show();
					cart_total.html(response.data.total);
					cart_count.html(response.data.count);

					$('#cart-content').append($('<div class="cart-just-added"></div>').html(f.find('span.added2cart').text()));
					if ($('#cart').hasClass('fixed')) {
						$('.cart-to-checkout').hide();
					}

					ftHighlight('#ft_cart');
				}
				else {
					// flying cart
					var imgParent = f.closest('.product_page').find('.product-gallery-wrap');
					var origin = f.closest('.product_page').find('.product-core-image');
					var block = $('<div class="fly_wrap"><div class="'+ origin.attr('class') +'"></div></div>').find('.product-core-image').append(origin.html());
					$('#ft_cart a').removeClass('empty');
					var ftCart = $('#ft_cart');
					var cart = $('#cart');
					var cartAnim = 0;

					if (ftCart.length) {
						cartAnim = {
							top: ftCart.offset().top,
							left: ftCart.offset().left,
							width: ftCart.width(),
							height: ftCart.width(),
							opacity: 0.4,
							speed: 2000
						};
					}
					else if (cart.length) {
						cartAnim = {
							top: cart.offset().top + 24,
							left: cart.offset().left + 24,
							width: 10,
							height: 10,
							opacity: 0.4,
							speed: 2000
						};
					}

					if (cartAnim) {
						if(origin.is(':visible')){
							var imgW = origin.width();
							var imgH = origin.height();
						}
						else{
							var imgW = imgParent.width();
							var imgH = imgParent.width();
						}

						block.css({
								'z-index': 101,
								background: '#fff',
								top: imgParent.offset().top,
								left: imgParent.offset().left,
								width: imgW + 'px',
								height: imgH + 'px',
								position: 'absolute',
								overflow: 'hidden'
							})
							.appendTo('body').css({
								'border': '1px solid #eee'
							})
							.supremate({
								top: cartAnim.top,
								left: cartAnim.left,
								width: cartAnim.width,
								height: cartAnim.height,
								opacity: cartAnim.opacity,
							}, cartAnim.speed, 'linear', function() {
								$(this).remove();
								cart_total.html(response.data.total);
								cart_count.html(response.data.count);
								ftHighlight('#ft_cart');
							});
					}
				}

				if (response.data.error) {
					alert(response.data.error);
				}
				
				if( typeof yaCounter49122622 != 'undefined' ) { yaCounter49122622.reachGoal('add_cart') }
				
			}
			else if (response.status == 'fail') {
				$.get( vars.shop.cart_url, function( data ) {
					var data = $("<div/>").html(data);

					var cart_total = $(".cart_total");
					var cart_count = $(".cart_count");
					var cart_total_val = data.find('#ajax-cart-total').html();
					var cart_count_val = data.find('#ajax-cart-count').html();

					if(cart_total.html() != cart_total_val && cart_count.html() != cart_count_val){
						cart_total.closest('.empty').removeClass('empty');
						cart_total.html(cart_total_val);
						cart_count.html(cart_count_val);
						ftHighlight('#ft_cart');
					}
				});
				alert(response.errors);
			}
		}, "json");

		return false;
	});
}

Product.prototype.checkFeatureAvailability = function(id) {

}

Product.prototype.setFeature = function(sku) {
	var self = this;
	if (sku) {
		if (sku.image_id) {
			$(".product-image-" + sku.image_id).click();
		}
		self.updateSkuServices(sku.id);
		if (sku.available) {
			self.button.removeAttr('disabled');
			self.b1cPluginButtons('show');
		}
		else {
			self.form.find("div.stocks div").hide();
			self.form.find(".sku-no-stock").show();
			self.button.attr('disabled', 'disabled');
			self.b1cPluginButtons('hide');
		}
		self.add2cart.find(".price").data('price', sku.price);
		self.updatePrice(sku.price, sku.compare_price);
	}
	self.cartButtonVisibility(true);
}

Product.prototype.currencyFormat = function(number, no_html) {
	// Format a number with grouped thousands
	//
	// +	 original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
	// +	 improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +	 bugfix by: Michael White (http://crestidg.com)

	var i, j, kw, kd, km;
	var decimals = this.currency.frac_digits;
	var dec_point = this.currency.decimal_point;
	var thousands_sep = this.currency.thousands_sep;

	// input sanitation & defaults
	if (isNaN(decimals = Math.abs(decimals))) {
		decimals = 2;
	}
	if (dec_point === undefined) {
		dec_point = ",";
	}
	if (thousands_sep === undefined) {
		thousands_sep = ".";
	}

	i = parseInt(number = (+number || 0).toFixed(decimals)) + "";

	if ((j = i.length) > 3) {
		j = j % 3;
	}
	else {
		j = 0;
	}

	km = (j ? i.substr(0, j) + thousands_sep : "");
	kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
	//kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
	kd = (decimals && (number - i) ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");

	number = km + kw + kd;
	var s = no_html ? this.currency.sign : this.currency.sign_html;
	if (!this.currency.sign_position) {
		return s + this.currency.sign_delim + number;
	}
	else {
		return number + this.currency.sign_delim + s;
	}
};

Product.prototype.serviceVariantHtml = function(id, name, price) {
	return $('<option data-price="' + price + '" value="' + id + '"></option>').text(name + ' (+' + this.currencyFormat(price, 1) + ')');
};

Product.prototype.updateSkuServices = function(sku_id) {
	artNumber = $('#sku' + sku_id).val();

	if (artNumber === '') {
		this.form.find('.articul').hide();
	}
	else {
		this.form.find('.artnumber').text(artNumber);
		this.form.find('.articul').show();
	}

	this.form.find("div.stocks").css('visibility', 'visible');
	this.form.find("div.stocks div").hide();
	this.form.find(".sku-" + sku_id + "-stock").show();
	for (var service_id in this.services[sku_id]) {
		var v = this.services[sku_id][service_id];
		if (v === false) {
			this.form.find(".service-" + service_id).hide().find('input,select').attr('disabled', 'disabled').removeAttr('checked');
		}
		else {
			this.form.find(".service-" + service_id).show().find('input').removeAttr('disabled');
			if (typeof(v) == 'string') {
				this.form.find(".service-" + service_id + ' .service-price').html(this.currencyFormat(v));
				this.form.find(".service-" + service_id + ' input').data('price', v);
			}
			else {
				var select = this.form.find(".service-" + service_id + ' .service-variants');
				var selected_variant_id = select.val();
				for (var variant_id in v) {
					var obj = select.find('option[value=' + variant_id + ']');
					if (v[variant_id] === false) {
						obj.hide();
						if (obj.attr('value') == selected_variant_id) {
							selected_variant_id = false;
						}
					}
					else {
						if (!selected_variant_id) {
							selected_variant_id = variant_id;
						}
						obj.replaceWith(this.serviceVariantHtml(variant_id, v[variant_id][0], v[variant_id][1]));
					}
				}

				this.form.find(".service-" + service_id + ' .service-variants').val(selected_variant_id);
			}
		}
	}
};

Product.prototype.updatePrice = function(price, compare_price) {
	if (price === undefined) {
		var input_checked = this.form.find(".skus input:radio:checked");
		if (input_checked.length) {
			price = parseFloat(input_checked.data('price'));
			compare_price = parseFloat(input_checked.data('compare-price'));
		}
		else {
			price = parseFloat(this.add2cart.find(".price").data('price'));
			compare_price = parseFloat(this.add2cart.find(".compare-at-price").data('compare-price'));
		}
	}

	var servicesPrice = 0;
	var self = this;
	this.form.find(".services input:checked").each(function() {
		var s = $(this).val();
		if (self.form.find('.service-' + s + ' .service-variants').length) {
			servicesPrice += parseFloat(self.form.find('.service-' + s + ' .service-variants :selected').data('price'));
		}
		else {
			servicesPrice += parseFloat($(this).data('price'));
		}
	});
	price += servicesPrice;

	this.add2cart.find(".price").html(this.currencyFormat(price));

	if (compare_price > 0 && servicesPrice === 0) {
		if (!this.prices.find(".compare-at-price").length) {
			this.prices.append('<span class="compare-at-price nowrap"></span>');
		}
		this.prices.find(".compare-at-price").html(this.currencyFormat(compare_price)).show();
		this.prices.find(".price").addClass('price-new');
		this.badgeWrapper.find(".badge.discount-percent").html('<span>-' + Math.round((compare_price - price) / (compare_price / 100)) + '%</span>');
		this.badgeWrapper.find(".badge.discount").show();
	}
	else {
		this.prices.find(".compare-at-price").hide();
		this.prices.find(".price").removeClass('price-new');
		this.badgeWrapper.find(".badge.discount").hide();
	}

};

Product.prototype.cartButtonVisibility = function(visible) {
	//toggles "Add to cart" / "%s is now in your shopping cart" visibility status
	if (visible) {
		// this.add2cart.find('.compare-at-price').show();
		this.add2cart.find('input[type="submit"]').show();
		this.add2cart.find('.price').show();
		this.add2cart.find('.qty').show();
		this.add2cart.find('span.added2cart').hide();
	}
	else {
		if (MatchMedia("only screen and (max-width: 760px)")) {
			this.add2cart.find('.compare-at-price').hide();
			this.add2cart.find('input[type="submit"]').hide();
			this.add2cart.find('.price').hide();
			this.add2cart.find('.qty').hide();
			this.add2cart.find('span.added2cart').show();
			if ($(window).scrollTop() >= 110)
				$('#cart').addClass('fixed');
			$('#cart-content').append($('<div class="cart-just-added"></div>').html(this.add2cart.find('span.added2cart').text()));
			if ($('#cart').hasClass('fixed'))
				$('.cart-to-checkout').slideDown(200);
		}
	}
};

Product.prototype.scrollToTab = function(tabID) {
	$('.pp_tabpanel a[href="' + tabID + '"]').tab('show');

	$('html, body').animate({
		scrollTop: $(".pp_tabpanel").offset().top
	}, 1000);
};

Product.prototype.moreImagesSliderDestroy = function() {
	this.moreImages.slick('unslick');
};

Product.prototype.moreImagesSlider = function() {
	var sidebarVisible = false;
	if($('.page-content').hasClass('w-sidebar') || $('.product_page').closest('.remodal').length){
		sidebarVisible = true;
	}

	var responsive = [{
		breakpoint: 1299,
		settings: {
			slidesToShow: sidebarVisible ? 4 : 6,
			slidesToScroll: sidebarVisible ? 4 : 6,
		}
	}, {
		breakpoint: 1199,
		settings: {
			slidesToShow: sidebarVisible ? 4 : 5,
			slidesToScroll: sidebarVisible ? 4 : 5,
		}
	}, {
		breakpoint: 991,
		settings: {
			slidesToShow: sidebarVisible ? 3 : 4,
			slidesToScroll: sidebarVisible ? 3 : 4
		}
	}, {
		breakpoint: 767,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 3
		}
	}, {
		breakpoint: 599,
		settings: {
			slidesToShow: 6,
			slidesToScroll: 6
		}
	}, {
		breakpoint: 499,
		settings: {
			slidesToShow: 5,
			slidesToScroll: 5
		}
	}, {
		breakpoint: 399,
		settings: {
			slidesToShow: 4,
			slidesToScroll: 4
		}
	}, {
		breakpoint: 0,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 3
		}
	}];

	this.moreImages.slick({
		speed: 500,
		touchThreshold: 8,
		mobileFirst: true,
		prevArrow: '<a class="slick-arrow slick-prev"><span class="fa fa-angle-left"></span></a>',
		nextArrow: '<a class="slick-arrow slick-next"><span class="fa fa-angle-right"></span></a>',
		infinite: false,
		slide: 'div.image',
		draggable: true,
		responsive: responsive
	});
};

Product.prototype.imageChange = function(imgId) {
	var self = this;
	var moreImg = self.moreImages.find('.product-image-' + imgId);
	var currentImgIndex = self.moreImages.find('.image.selected').attr('data-slick-index');
	var newImgIndex = moreImg.parent().attr('data-slick-index');
	var slidesCount = self.moreImages.find('.image').length;

	if(newImgIndex != currentImgIndex){
		self.video.hide();
		self.coreImage.show();

		self.moreImages.find('.image').removeClass('selected');
		moreImg.parent().addClass('selected');

		this.moreImages.slick('slickGoTo', newImgIndex);

		self.productImage.addClass('blurred');
		self.coreImage.find(".switching-image").show();

		var img = moreImg.find('img');
		var size = self.productImage.attr('src').replace(/^.*\/[^\/]+\.(.*)\.[^\.]*$/, '$1');
		var src = img.attr('src').replace(/^(.*\/[^\/]+\.)(.*)(\.[^\.]*)$/, '$1' + size + '$3');
		$('<img>').attr('src', src).load(function() {
			self.productImage.attr('src', src);
			self.productImage.removeClass('blurred');
			self.coreImage.find(".switching-image").hide();
		}).each(function() {
			//ensure image load is fired. Fixes opera loading bug
			if (this.complete) {
				$(this).trigger("load");
			}
		});
		size = self.productImage.parent().attr('href').replace(/^.*\/[^\/]+\.(.*)\.[^\.]*$/, '$1');
		var href = img.attr('src').replace(/^(.*\/[^\/]+\.)(.*)(\.[^\.]*)$/, '$1' + size + '$3');
		self.productImage.parent().attr('href', href);
	}
};

Product.prototype.imagePopup = function() {
	var self = this;
	if (self.coreImage.find('a').length) {
		self.coreImage.find('a').on('click', function(e) {
			e.preventDefault();

			var thisImage = $(this);
			var startIndex = 0;

			//var productImagesGal = JSON.stringify(self.moreImages.data('thumbs'));
			var productImagesGal = self.moreImages.data('thumbs');
			if (typeof productImagesGal != 'undefined') {
				$.each(productImagesGal, function(index, value) {
					if (value.src == thisImage.attr('href')) {
						startIndex = index;
					}
				});
				galItems = productImagesGal;
			}
			else {
				galItems = this;
			}

			$.magnificPopup.open({
				items: galItems,
				type: 'image',
				gallery: {
					enabled: true,
					preload: [0, 2],
				},
				closeBtnInside: true,
				// mainClass: 'mfp-zoom-in mfp-with-anim'

				callbacks: {
					beforeOpen: function() {
						// just a hack that adds mfp-anim class to markup
						this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
						this.st.mainClass = 'mfp-zoom-in';

						// $('.remodal, .remodal-bg').addClass('blur');
						$('.remodal-bg').addClass('blur');

						fixScroll('show');
					},
					close: function() {
						$('.remodal, .remodal-bg').removeClass('blur');
						if (!$('.remodal.remodal-is-opened').length) {
							fixScroll('hide');
						}
					}
				}
			}, startIndex);

			return false;
		});
	}
};

Product.prototype.b1cPluginButtons = function(state) {
	var self = this;
	if(self.isQuickView){
		if(state == 'show'){
			self.buttonStorequickorder.show();
			self.buttonQuickorder.show();
		}
		else{
			self.buttonStorequickorder.hide();
			self.buttonQuickorder.hide();
		}
	}
};
