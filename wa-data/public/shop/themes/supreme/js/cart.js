/* supreme 1.4.8.4 */
$(function() {

	function updateCart(data) {
		$(".cart-total").html(data.total);
		$(".cart_total").html(data.total).closest('#cart').removeClass('empty');
		$(".cart_count").html(data.count);

		if (data.discount_numeric) {
			$(".cart-discount").closest('.item').show();
		}
		$(".cart-discount").html('&minus;' + data.discount);
		$(".coupon_discount").html('&minus;' + data.discount_coupon);

		if (data.add_affiliate_bonus) {
			$(".affiliate").show().html('<span class="fa fa-thumbs-o-up like"></span> ' + data.add_affiliate_bonus);
		}
		else {
			$(".affiliate").hide();
		}

		/*
		Плагин "Гибкие скидки и бонусы"
		https://www.webasyst.ru/store/plugin/shop/flexdiscount/
		*/
		if (typeof $.flexdiscountFrontend !== 'undefined') {
			$.flexdiscountFrontend.cartChange();
		}
	}

	$(".cart a.delete").click(function() {
		var row = $(this).closest('.item');
		$.post('delete/', {
			html: 1,
			id: row.data('id')
		}, function(response) {
			if (response.data.count === 0) {
				location.reload();
			}
			row.remove();
			updateCart(response.data);
		}, "json");
		return false;
	});

	$(".btn_clear_cart").click(function() {
		for (var i = 0; i < inCart.length; i++) {
			$.post('delete/', {
				html: 1,
				id: inCart[i]
			}, function(response) {
				if (response.data.count === 0) {
					location.reload();
				}
			}, "json");
		}
	});

	function cartQty(el) {
		var that = $(el);
		var row = that.closest('.item');
		if (that.val()) {
			$.post('save/', {
				html: 1,
				id: row.data('id'),
				quantity: that.val()
			}, function(response) {
				row.find('.item-total > span').html(response.data.item_total);
				if (response.data.q) {
					that.val(response.data.q);
				}
				if (response.data.error) {
					alert(response.data.error);
				}
				else {
					that.removeClass('error');
				}
				updateCart(response.data);
				// Товары-комплекты (https://www.webasyst.ru/store/plugin/shop/itemsets/)
				if (typeof $.itemsetsFrontend !== 'undefined') {
					$.itemsetsFrontend.quantityChange(that);
				}
			}, "json");
		}
	}

	$(".cart input.qty").change(function() {
		cartQty(this);
	});

	$(".cart .services input:checkbox").change(function() {
		var obj = $('select[name="service_variant[' + $(this).closest('.item').data('id') + '][' + $(this).val() + ']"]');
		if (obj.length) {
			if ($(this).is(':checked')) {
				obj.removeAttr('disabled');
			}
			else {
				obj.attr('disabled', 'disabled');
			}
		}

		var div = $(this).closest('div');
		var row = $(this).closest('.item');
		if ($(this).is(':checked')) {
			var parent_id = row.data('id');
			var data = {
				html: 1,
				parent_id: parent_id,
				service_id: $(this).val()
			};
			var $variants = $('[name="service_variant[' + parent_id + '][' + $(this).val() + ']"]');
			if ($variants.length) {
				data.service_variant_id = $variants.val();
			}
			$.post('add/', data, function(response) {
				div.data('id', response.data.id);
				row.find('.item-total > span').html(response.data.item_total);
				updateCart(response.data);
			}, "json");
		}
		else {
			$.post('delete/', {
				html: 1,
				id: div.data('id')
			}, function(response) {
				div.data('id', null);
				row.find('.item-total > span').html(response.data.item_total);
				updateCart(response.data);
			}, "json");
		}
	});

	$(".cart .services select").change(function() {
		var row = $(this).closest('.item');
		$.post('save/', {
			html: 1,
			id: $(this).closest('div').data('id'),
			'service_variant_id': $(this).val()
		}, function(response) {
			row.find('.item-total > span').html(response.data.item_total);
			updateCart(response.data);
		}, "json");
	});

	$("#cancel-affiliate").click(function() {
		$(this).closest('form').append('<input type="hidden" name="use_affiliate" value="0">').submit();
		return false;
	});

	$("#use-coupon").click(function() {
		$('#discount-row:hidden').slideToggle(200);
		$('#discount-row').addClass('highlighted');
		$('#apply-coupon-code:hidden').show();
		$('#apply-coupon-code input[type="text"]').focus();
		return false;
	});

});
