/* supreme 1.4.8.4 */
function ftHighlight(target) {
	if (target) {
		target = $(target);
		target.addClass('added');
		setTimeout(function() {
			target.removeClass('added');
		}, 1000);
	}
}

function pSetList(el, listname, action, hide, disableHighlight) {
	var $this = $(el);
	var list = $.cookie('shop_' + listname);
	var pID = $this.data('product');

	// remove
	if (action == 'remove') {
		if (list) {
			list = list.split(',');
		} else {
			list = [];
		}

		var i = $.inArray(pID + '', list);

		if (i != -1) {
			list.splice(i, 1);
		}

		if (list.length === 0) {
			$.removeCookie('shop_' + listname, {
				path: '/'
			});
		} else {
			$.cookie('shop_' + listname, list.join(','), {
				expires: 30,
				path: '/'
			});
		}

		$('.to_' + listname + '[data-product="' + pID + '"]').removeClass('active');
		$('#ft_' + listname + ' a').find('.count').html(list.length);

		if (hide === true) {
			var parentUl = $this.parents('.product-list').find('.product').length;
			$this.closest('.product').fadeOut(300).remove();
			if (parentUl == 1) {
				window.location.reload();
			}
		}
	}
	// add
	else {
		if (list && list !== null && list !== '') {
			list = pID + ',' + list;
		} else {
			list = '' + pID;
		}

		$('.to_' + listname + '[data-product="' + pID + '"]').addClass('active');
		$('#ft_' + listname + ' a').find('.count').html(list.split(',').length);

		$.cookie('shop_' + listname, list, {
			expires: 30,
			path: '/'
		});
	}

	if (!disableHighlight) {
		ftHighlight('#ft_' + listname);
	}
}


function sidePluginLimit(parent, target) {
	parent = $(parent);
	var el = parent.find(target);
	var limit = parent.attr('data-limit');
	var items = el.children();

	if (items.length > limit) {
		items.slice(limit, items.length).hide();
		parent.find('.show_more').show();

		showItems(el);
	}
}

$(document).ready(function() {

	function hideSearchResult(){
		$('.search-result').removeClass('is-visible').html('');
		$(window).off("throttledresize.searchResult");
		$(document).off('click.searchResult');
	}

	function showSearchResult(el){
		var parent = $(el).closest('.search');
		var offset = parent.offset();
		var width = parent.width();
		var height = parent.height();

		$('.search-result').css({
			"width": width,
			"top": offset.top + height,
			"left": offset.left
		}).addClass('is-visible');

		$(window).on('throttledresize.searchResult', function(){
			var offset = parent.offset();
			if(parent.is(':visible')){
				$('.search-result').css({
					"width": parent.width(),
					"top": offset.top + parent.height(),
					"left": offset.left
				});
			}
			else{
				hideSearchResult();
			}
		});

		$(document).on('click.searchResult', function(e){
			if(!$(e.target).hasClass('search') && !$(e.target).parents().hasClass('search') && !$(e.target).hasClass('search-result') && !$(e.target).parents().hasClass('search-result')){
				hideSearchResult();
			}
		});
	}

	function loadAjaxSearch(el){
		var self = $(el);
		var parent = self.closest('.search');
		var query = self.val();
		var url = parent.attr('action');

		$.ajax({
			url: url,
			method:"GET",
			data:{query:query, ajax:true},
			success:function(data)
			{
				if($(data).length){
					$('.search-result').html(data);
					showSearchResult(el);
				}
				else{
					hideSearchResult();
				}
			}
		});
	}

	if(vars.enable_ajax_search){
		$('.search .search-field').focus(function(){
			if( $(this).val().length >= 3 ){
				if(!$('.search-result').children().length){
					loadAjaxSearch(this);
				}
				showSearchResult(this);
			}
		});

		var typingTimer;
		$('.search .search-field').keyup(function(){
			clearTimeout(typingTimer);
			var self = $(this);
			var parent = self.closest('.search');
			var query = $(this).val();

			if(query !== '' && query.length >= 3){
				typingTimer = setTimeout(function(){
					loadAjaxSearch(self);
				}, 500);
			}
			else{
				hideSearchResult();
			}
		});
	}


	showItems('.side_tags');

	sidePluginLimit('.side_block .brands-plugin', 'ul');
	sidePluginLimit('.side_block .productbrands-plugin', 'ul');

	// COUNTDOWN
	if ($.fn.countdowntimer) {
		$('.js-promo-countdown').each(function() {
			var $this = $(this).html('');
			var id = ($this.attr('id') || 'js-promo-countdown' + ('' + Math.random()).slice(2));
			$this.attr('id', id);
			var start = $this.data('start').replace(/-/g, '/');
			var end = $this.data('end').replace(/-/g, '/');
			$this.countdowntimer({
				startDate: start,
				dateAndTime: end,
				size: 'lg'
			});
		});
	}

	// HOME SLIDER
	var homeSlider;
	var homeSliderSettingsDefaults = {
		// fade: true,
		autoplay: true,
		autoplaySpeed: 4000,
		speed: 500,
		touchThreshold: 16,
		mobileFirst: true,
		dots: true,
		prevArrow: '<a class="slick-arrow slick-prev"><span class="fa fa-angle-left"></span></a>',
		nextArrow: '<a class="slick-arrow slick-next"><span class="fa fa-angle-right"></span></a>',
		infinite: true,
		slide: 'div.item',
		draggable: true,
	};

	var homeSliderSettings;

	function hsSetSize(){
		var wWidth = $('.homeslider').parent().width();
		var newOuterHeight = $('.homeslider').width() / hsRatio;
		var newHeight = $('.homeslider').width() / hsRatio;
		var scale = newHeight / hsHeight;

		var hsItem = $('.homeslider .item');
		var hsItemIn = $('.homeslider .item > *');

		var hsDots = $('.homeslider .slick-dots');

		if(wWidth < hsWidth){
			homeSlider.height(newOuterHeight);
			hsItem.height(newOuterHeight);
			var width = hsWidth + 'px';

			// if(window.matchMedia("screen and (max-width: 767px)").matches){
			// 	width = 1150 - (30 / scale) + 'px';
			// }
			// else{
			// 	width = 1150 - (160 / scale) + 'px';
			// }
			// width = hsWidth + 'px';
			hsItemIn.css({
				width: width,
				height: hsHeight,
				transform: "translate(-50%, -50%) " + "scale(" + scale + ")"
			});

			hsDots.css({transform: "scale(" + scale + ")"});
		}
		else{
			homeSlider.height(hsHeight);
			hsItem.height(hsHeight);

			hsItemIn.css({
				width: "",
				height: "",
				transform: ""
			});

			hsDots.css({transform: ""});
		}
	}

	if($('#homeslider_products').length){
		homeSlider = $('#homeslider_products .slick-slider');

		homeSlider.on('afterChange', function(event, slick, currentSlide) {
			homeSlider.find('div[data-slick-index=' + currentSlide + ']').css('z-index', '1').siblings().css('z-index', '');
		});

		if((typeof homeSliderSettingsCustom != 'undefined')){
			homeSliderSettings = $.extend({}, homeSliderSettingsDefaults, homeSliderSettingsCustom);
		}
		else{
			homeSliderSettings = homeSliderSettingsDefaults;
		}

		homeSlider.slick(homeSliderSettings);
	}

	else if($('#homeslider_photos').length){
		var homeSliderPhotos = $('#homeslider_photos');
		homeSlider = $('#homeslider_photos .slick-slider');

		// variable vars form index.head.html
		var hsWidth = (typeof vars.site_max_width !== 'undefined' ? vars.site_max_width : 1200);

		if($('.homeslider').hasClass('stacked')){
			hsWidth = (typeof vars.content_width !== 'undefined' ? vars.content_width : 893);
		}

		var hsHeight = parseInt($('.homeslider').attr('data-height').replace('px',''));
		if(!hsHeight){
			hsHeight = 360;
		}

		var hsRatio = hsWidth / hsHeight;

		if((typeof homeSliderSettingsCustom != 'undefined')){
			homeSliderSettings = $.extend({}, homeSliderSettingsDefaults, homeSliderSettingsCustom);
		}
		else{
			homeSliderSettings = homeSliderSettingsDefaults;
		}

		homeSliderPhotos.on('init', function(){
			homeSliderPhotos.addClass('init');
		});

		homeSlider.on('afterChange', function(event, slick, currentSlide) {
			homeSlider.find('div[data-slick-index=' + currentSlide + ']').css('z-index', '1').siblings().css('z-index', '');
		});

		homeSlider.slick(homeSliderSettings);

		hsSetSize();

		$(window).on('throttledresize',function(){
			hsSetSize();
		});
	}


	// PRODUCTS SLIDER
	var psWrapper = $('.products_slider');
	if (psWrapper.length) {
		psWrapper.each(function() {
			var self = $(this);
			var pl = self.find('.product-list');
			var pr = pl.data('per-row');
			var maxItems = ($('.sidebar').length) ? pr : pr + 1;

			self.prepend('<div class="slick-arrows"/>');

			var psResponsive = [{
				breakpoint: 1199,
				settings: {
					slidesToShow: maxItems,
					slidesToScroll: maxItems,
				}
			}, {
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}, {
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}, {
				breakpoint: 599,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}, {
				breakpoint: 0,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}];

			var sliderConf = {
				speed: 500,
				touchThreshold: 8,
				appendArrows: self.find('.slick-arrows'),
				// cssEase: 'easeInCubic',
				mobileFirst: true,
				prevArrow: '<a class="slick-arrow slick-prev"><span class="fa fa-angle-left"></span></a>',
				nextArrow: '<a class="slick-arrow slick-next"><span class="fa fa-angle-right"></span></a>',
				infinite: true,
				slide: 'div.product',
				draggable: false,
				customPaging: "20px",
				responsive: psResponsive
			};

			var sliderCustomConf = self.data('slider-conf');
			if(sliderCustomConf){
				sliderConf = $.extend( {}, sliderConf, sliderCustomConf );
			}

			pl.slick(sliderConf);
		});
	}


	// CATEGORY SLIDER
	var csWrapper = $('.categories_slider');
	if (csWrapper.length) {
		csWrapper.each(function() {
			var self = $(this);
			var pl = self.find('.categories_grid');
			var pr = pl.data('per-row');
			var maxItems = ($('.sidebar').length) ? pr : pr + 1;

			self.prepend('<div class="slick-arrows"/>');

			var psResponsive = [{
				breakpoint: 1199,
				settings: {
					slidesToShow: maxItems,
					slidesToScroll: maxItems,
				}
			}, {
				breakpoint: 991,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}, {
				breakpoint: 767,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}, {
				breakpoint: 599,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}, {
				breakpoint: 0,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}];

			var sliderConf = {
				speed: 500,
				touchThreshold: 8,
				appendArrows: self.find('.slick-arrows'),
				// cssEase: 'easeInCubic',
				mobileFirst: true,
				prevArrow: '<a class="slick-arrow slick-prev"><span class="fa fa-angle-left"></span></a>',
				nextArrow: '<a class="slick-arrow slick-next"><span class="fa fa-angle-right"></span></a>',
				infinite: true,
				slide: 'div.item',
				draggable: false,
				customPaging: "20px",
				responsive: psResponsive
			};

			var sliderCustomConf = self.data('slider-conf');
			if(sliderCustomConf){
				sliderConf = $.extend( {}, sliderConf, sliderCustomConf );
			}

			pl.slick(sliderConf);
		});
	}


	// CURRENCY DROPDOWN
	$(".currency-toggle ul a").on('click', function() {
		var url = window.location.search;

		if (url.indexOf('?') == -1) {
			url += '?';
		} else {
			url += '&';
		}
		window.location.href = url + 'currency=' + $(this).data('value');
	});

	// CLEAR LIST
	$(document).on('click', '.btn_clear_list', function() {
		$.removeCookie('shop_' + $(this).data('target'), {
			path: '/'
		});
		window.location.reload();
	});

	// FAVORITES
	$(document).on('click', 'a.to_favorites', function() {
		var listname = 'favorites';
		if (!$(this).hasClass('active')) {
			pSetList(this, listname, 'add');
		} else {
			if ($('.products_list_favorites').length) {
				pSetList(this, listname, 'remove', true); //remove from list and page
			} else {
				pSetList(this, listname, 'remove');
			}
		}
		return false;
	});

	// FAVORITES - remove
	$(document).on('click', '.products_list_favorites .btn_remove', function() {
		pSetList(this, 'favorites', 'remove', true); //remove from list and page
	});


	// VIEWED
	if ($('.product_page').length) {
		var viewed = $.cookie('shop_viewed');
		var limit = 100;

		if (typeof productID !== 'undefined') {
			if (viewed && viewed !== '') {
				var viewedArr = viewed.split(',');
				var inArr = $.inArray(productID.toString(), viewedArr);

				if (viewed && inArr == -1) {
					if(viewedArr.length >= limit){
						viewed = viewedArr.slice(0, limit-1).join(',');
					}

					viewed =  productID + ',' + viewed;
				}
				$.cookie('shop_viewed', viewed, {
					expires: 30,
					path: '/'
				});
			}
			else {
				$.cookie('shop_viewed', productID.toString(), {
					expires: 30,
					path: '/'
				});
			}
		}
	}

	// VIEWED - remove
	$(document).on('click', '.products_list_viewed .btn_remove', function() {
		pSetList(this, 'viewed', 'remove', true); //remove from list and page
	});


	// COMPARE
	$(document).on('click', 'a.to_compare', function() {
		var listname = 'compare';
		if (!$(this).hasClass('active')) {
			pSetList(this, listname, 'add');
		} else {
			pSetList(this, listname, 'remove');
		}
		return false;
	});

	// COMPARE - REMOVE
	$(document).on('click', '#compare-table .btn_remove', function() {
		pSetList(this, 'compare', 'remove', false, true); //remove from list and page
		window.location.reload();
	});


	$(document).on('click', '#compare-all', function() {
		$("#compare-table tr.same").show();
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		return false;
	});

	$(document).on('click', '#compare-diff', function() {
		$("#compare-table tr.same").hide();
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		return false;
	});

	//CART dialog for multi-SKU products
	$('.dialog').on('click', 'a.dialog-close', function() {
		$(this).closest('.dialog').hide().find('.cart').empty();
		return false;
	});
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			$(".dialog:visible").hide().find('.cart').empty();
		}
	});

	//QUICK PHOTO
	$(document).on('click', '.product-list .quick_photo', function(e) {
		var self = $(this);
		var name = $(this).data('name');
		var url = $(this).data('url');
		e.preventDefault();
		var productImagesGal = JSON.parse(self.attr('data-images'));

		$.magnificPopup.open({
			items: productImagesGal,
			type: 'image',
			gallery: {
				enabled: true,
				preload: [0, 2],
			},
			closeBtnInside: true,
			callbacks: {
				beforeOpen: function() {
					this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
					this.st.mainClass = 'mfp-zoom-in';
					$('.remodal, .remodal-bg').addClass('blur');
					fixScroll('show');
				},
				close: function() {
					$('.remodal, .remodal-bg').removeClass('blur');
					fixScroll('hide');
				}
			}
		});
	});

	//QUICK VIEW remodal
	var qvRemodal = $('[data-remodal-id=remodal_quick_view]');
	var qvRemodalObj = qvRemodal.remodal({
		hashTracking: false
	});

	$(document).on('quick_view_added_to_basket', qvRemodal, function() {
		qvRemodalObj.close();
	});

	$(document).on('closed', qvRemodal, function() {
		qvRemodal.find('.remodal_in').html('');
	});

	$(document).on('click', '.product-list .quick_view', function() {
		var qvBtn = $(this);

		if (qvBtn.data('url')) {
			var fromPP = ($('.product_page').length) ? '&from_product_page=1' : '';
			qvRemodal.hide();
			fixScroll('show');
			qvRemodalObj.open();

			$.get(qvBtn.data('url') + fromPP, function(data) {
				$('[data-remodal-id=remodal_quick_view] .remodal_in').html($(data).find('.product_page'));
				qvRemodal.show();
			});
			return false;
		}
	});

	//ADD TO CART
	$(document).on('submit', '.product form.addtocart', function() {
		var f = $(this);
		f.find('.adding2cart').addClass('icon16 loading').show();

		if (f.data('url')) {
			var fromPP = ($('.product_page').length) ? '&from_product_page=1' : '';
			$.get(f.data('url') + fromPP, function(data) {
				$('[data-remodal-id=remodal_quick_view] .remodal_in').html($(data).find('.product_page'));
				var qvRemodal = $('[data-remodal-id=remodal_quick_view]').remodal({
					hashTracking: false
				});
				fixScroll('show');
				qvRemodal.open();
				f.find('.adding2cart').hide();
			});
			return false;
		}

		$.post(f.attr('action') + '?html=1', f.serialize(), function(response) {
			f.find('.adding2cart').hide();

			if (response.status == 'ok') {
				var cart_total = $(".cart_total");
				var cart_count = $(".cart_count");

				cart_total.closest('#cart').removeClass('empty');
				$('#ft_cart a').removeClass('empty');

				if (MatchMedia("only screen and (max-width: 767px)")) {
					f.find('span.added2cart').show();
					cart_total.html(response.data.total);
					cart_count.html(response.data.count);
					ftHighlight('#ft_cart');
				}
				else {
					// flying cart
					var ftCart = $('#ft_cart');
					var cart = $('#cart');
					var cartAnim = 0;

					if (ftCart.length) {
						cartAnim = {
							top: ftCart.offset().top,
							left: ftCart.offset().left,
							width: ftCart.width(),
							height: ftCart.width(),
							opacity: 0.4,
							speed: 1200
						};
					} else if (cart.length) {
						cartAnim = {
							top: cart.offset().top + 24,
							left: cart.offset().left + 24,
							width: 10,
							height: 10,
							opacity: 0.4,
							speed: 2000
						};
					}

					var origin = f.closest('.product');
					var originIn = origin.find('.in');

					if (cartAnim && originIn.length) {
						var parentClass = '';

						if (origin.closest('.product-list').hasClass('products_view_grid')) {
							parentClass = 'products_view_grid';
						} else if (origin.parent().hasClass('products_view_list')) {
							parentClass = 'products_view_list';
						}
						var block = $('<div class="fly_wrap product-list ' + parentClass + '"></div>').append('<div class="product">' + origin.html() + '</div>');

						block.css({
								'z-index': 101,
								background: '#fff',
								top: originIn.offset().top,
								left: originIn.offset().left,
								width: originIn.width() + 'px',
								height: originIn.height() + 'px',
								position: 'absolute',
								overflow: 'hidden'
							})
							.appendTo('body')
							.supremate({
								top: cartAnim.top,
								left: cartAnim.left,
								width: cartAnim.width,
								height: cartAnim.height,
								opacity: cartAnim.opacity,
							}, cartAnim.speed, 'linear', function() {
								$(this).remove();
								cart_total.html(response.data.total);
								cart_count.html(response.data.count);
								ftHighlight('#ft_cart');
							});
					}
					else{
						cart_total.html(response.data.total);
						cart_count.html(response.data.count);
						ftHighlight('#ft_cart');
					}
				}

				if ($('.cart-summary-page').length) {
					window.location.reload();
				}

				if (response.data.error) {
					alert(response.data.error);
				}
				
				if( typeof yaCounter49122622 != 'undefined' ) { yaCounter49122622.reachGoal('add_cart') }
				
			}
			else if (response.status == 'fail') {
				alert(response.errors);
				$.get( vars.shop.cart_url, function( data ) {
					data = $("<div/>").html(data);

					var cart_total = $(".cart_total");
					var cart_count = $(".cart_count");
					var cart_total_val = data.find('#ajax-cart-total').html();
					var cart_count_val = data.find('#ajax-cart-count').html();

					if(cart_total.html() != cart_total_val && cart_count.html() != cart_count_val){
						cart_total.closest('.empty').removeClass('empty');
						cart_total.html(cart_total_val);
						cart_count.html(cart_count_val);
						ftHighlight('#ft_cart');
					}
				});
			}
		}, "json");
		return false;
	});


	// MOBILE FILTER
	function mFilter(close) {
		var $f = $('.filters');
		var $fm = $('#fm');
		var $fmb = $('#filter_mobile_btn');
		var $fmr = $('[data-remodal-id=remodal_filter]');
		var $fmrObj = $fmr.remodal({
			hashTracking: false
		});

		if (close) {
			$fmrObj.close();
		} else {
			$(document).on('click', '#filter_mobile_btn', function() {
				if (!$fm.hasClass('open')) {
					$f.appendTo($fm);
					$fmrObj.open();
				}
			});

			$(document).on('closed', $fmr, function(e) {
				$f.appendTo('.side_filter_wrap');
			});
		}
	}
	mFilter();


	// PRODUCT FILTERING
	function filter() {
		function ajax_form_callback(f) {
			var fields = f.serializeArray();
			var params = [];
			for (var i = 0; i < fields.length; i++) {
				if (fields[i].value !== '') {
					params.push(fields[i].name + '=' + fields[i].value);
				}
			}
			var url = '?' + params.join('&');
			if ($(window).lazyLoad) {
				$(window).lazyLoad('sleep');
			}

			$('#product-list').addClass('loading');
			$.get(url + '&_=_', function(html) {
				var tmp = $('<div></div>').html(html);
				$('#product-list').html(tmp.find('#product-list').html()).removeClass('loading');

				/* Плиточная галерея */
				if(typeof $.pluginprotilegallery != "undefined"){
					$.pluginprotilegallery.lazyload();
				}

				if (!!(history.pushState && history.state !== undefined)) {
					window.history.pushState({}, '', url);
				}
				if ($(window).lazyLoad) {
					$(window).lazyLoad('reload');
				}
			});
		}

		$('.filters.ajax form input').change(function() {
			ajax_form_callback($(this).closest('form'));
		});
		$('.filters.ajax form').submit(function() {
			ajax_form_callback($(this));
			if ($(this).closest('remodal')) {
				mFilter(true); // close remodal
			}
			return false;
		});

		$('.filters .slider').each(function() {
			if (!$(this).find('.filter-slider').length) {
				$(this).append('<div class="filter-slider"></div>');
			} else {
				return;
			}
			var min = $(this).find('.min');
			var max = $(this).find('.max');
			var min_value = parseFloat(min.attr('placeholder'));
			var max_value = parseFloat(max.attr('placeholder'));
			var step = 1;
			var slider = $(this).find('.filter-slider');
			if (slider.data('step')) {
				step = parseFloat(slider.data('step'));
			} else {
				var diff = max_value - min_value;
				if (Math.round(min_value) != min_value || Math.round(max_value) != max_value) {
					step = diff / 10;
					var tmp = 0;
					while (step < 1) {
						step *= 10;
						tmp += 1;
					}
					step = Math.pow(10, -tmp);
					tmp = Math.round(100000 * Math.abs(Math.round(min_value) - min_value)) / 100000;
					if (tmp && tmp < step) {
						step = tmp;
					}
					tmp = Math.round(100000 * Math.abs(Math.round(max_value) - max_value)) / 100000;
					if (tmp && tmp < step) {
						step = tmp;
					}
				}
			}
			slider.slider({
				range: true,
				min: parseFloat(min.attr('placeholder')),
				max: parseFloat(max.attr('placeholder')),
				step: step,
				values: [parseFloat(min.val().length ? min.val() : min.attr('placeholder')),
					parseFloat(max.val().length ? max.val() : max.attr('placeholder'))
				],
				slide: function(event, ui) {
					var v = ui.values[0] == $(this).slider('option', 'min') ? '' : ui.values[0];
					min.val(v);
					v = ui.values[1] == $(this).slider('option', 'max') ? '' : ui.values[1];
					max.val(v);
				},
				stop: function(event, ui) {
					min.change();
				}
			});
			min.add(max).change(function() {
				var v_min = min.val() === '' ? slider.slider('option', 'min') : parseFloat(min.val());
				var v_max = max.val() === '' ? slider.slider('option', 'max') : parseFloat(max.val());
				if (v_max >= v_min) {
					slider.slider('option', 'values', [v_min, v_max]);
				}
			});
		});
	}
	filter();

	// CATALOG - SELECT VIEW
	$(document).on('click', '.select_view button:not(".active")', function() {
		val = $(this).val();
		$.cookie('catalog_view', val, {
			expires: 30,
			path: '/'
		});
		$(this).siblings().removeClass('active');
		$(this).addClass('active');

		$('#product-list').addClass('loading');
		$.post(window.location.href, function(html) {
			var tmp = $('<div></div>').html(html);
			$('#product-list').html(tmp.find('#product-list').html()).removeClass('loading');

			/* Плиточная галерея */
			if(typeof $.pluginprotilegallery != "undefined"){
				$.pluginprotilegallery.lazyload();
			}

			if ($(window).lazyLoad) {
				$(window).lazyLoad('reload');
			}
		});
		return false;
	});

	// CATALOG - PRODUCTS PER PAGE
	$(document).on('click', '.per_page_wrap ul a', function(e) {
		e.preventDefault();
		var $this = $(this);
		if (!$this.parent().hasClass('active') && typeof $this.data('val') == 'number') {
			$('#per_page_dropdown span').text($this.data('val'));
			$.cookie('products_per_page', $this.data('val'), {
				expires: 30,
				path: '/'
			});

			var url = window.location.search.replace(/(page=)(\w+)/g, 'page=1');

			$('#product-list').addClass('loading');
			$.post(url, function(html) {
				var tmp = $('<div></div>').html(html);
				$('#product-list').html(tmp.find('#product-list').html()).removeClass('loading');

				/* Плиточная галерея */
				if(typeof $.pluginprotilegallery != "undefined"){
					$.pluginprotilegallery.lazyload();
				}

				if (!!(history.pushState && history.state !== undefined)) {
					window.history.pushState({}, '', url);
				}

				if ($(window).lazyLoad) {
					$(window).lazyLoad('reload');
				}
			});
		}
	});


	// CATALOG - SORT
	$(document).on('click', '.sort_wrap ul a', function(e) {
		e.preventDefault();
		var url = $(this).attr('href');

		if(url == window.location.pathname){
			url = window.location.search
				.replace(/(page=)(\w+)/g, 'page=1')
				.replace(/(sort|order)=(\w+)/g, '') // remove order&sort
				.replace(/(\&+\&)/g, '&') // replace && with &
				.replace(/&+$/g, ''); // last &
		}
		else{
			url = url.replace(/(page=)(\w+)/g, 'page=1');
		}

		window.location.href = url;

		// url = href;
		// $('#product-list').addClass('loading');
		// $.get(url, function(html) {
		// 	var tmp = $('<div></div>').html(html);
		//
		// 	if (!!(history.pushState && history.state !== undefined)) {
		// 		window.history.pushState({}, '', url);
		// 	}
		//
		// 	$('#product-list').html(tmp.find('#product-list').html()).removeClass('loading');
		// 	if ($(window).lazyLoad) {
		// 		$(window).lazyLoad('reload');
		// 	}
		// });
	});

	// QUANTITY
	$('input[name^=quantity]').numeric({
		decimal: false,
		negative: false
	});
	$(document).on('click', '.qty_wrap .btn', function() {
		quantity = $(this).closest('.qty_wrap').find('input');
		val = parseInt(quantity.val());
		if ($(this).hasClass('minus')) {
			if (val > 1) {
				quantity.val(val - 1).change();
			}
		} else if ($(this).hasClass('plus')) {
			quantity.val(val + 1).change();
		}
	});

	//LAZYLOADING
	if ($.fn.lazyLoad) {
		var paging = $('.lazyloading-paging');
		if (!paging.length) {
			return;
		}

		var times = parseInt(paging.data('times'), 10);
		var link_text = paging.data('linkText') || 'Load more';
		var loading_str = paging.data('loading-str') || 'Loading...';

		// check need to initialize lazy-loading
		var current = paging.find('li.selected');
		if (current.children('a').text() != '1') {
			return;
		}
		paging.hide();
		var win = $(window);

		// prevent previous launched lazy-loading
		win.lazyLoad('stop');

		// check need to initialize lazy-loading
		var next = current.next();
		if (next.length) {
			win.lazyLoad({
				container: '#product-list .product-list',
				load: function() {
					win.lazyLoad('sleep');

					var paging = $('.lazyloading-paging').hide();

					// determine actual current and next item for getting actual url
					var current = paging.find('li.selected');
					var next = current.next();
					var url = next.find('a').attr('href');
					if (!url) {
						win.lazyLoad('stop');
						return;
					}

					var product_list = $('#product-list .product-list');
					// var loading = paging.parent().find('.loading').parent();
					// if (!loading.length) {
					// 	loading = $('<div><i class="icon16 loading"></i>' + loading_str + '</div>').insertBefore(paging);
					// }

					// loading.show();
					$.get(url, function(html) {
						var tmp = $('<div></div>').html(html);
						if ($.Retina) {
							tmp.find('#product-list .product-list img').retina();
						}
						product_list.append(tmp.find('#product-list .product-list').children());
						var tmp_paging = tmp.find('.lazyloading-paging').hide();
						paging.replaceWith(tmp_paging);
						paging = tmp_paging;

						times -= 1;

						// check need to stop lazy-loading
						var current = paging.find('li.selected');
						var next = current.next();
						if (next.length) {
							if (!isNaN(times) && times <= 0) {
								win.lazyLoad('sleep');
								if (!$('.lazyloading-load-more').length) {
									$('<div class="lazyloading-wrap"><a href="#" class="btn btn-default lazyloading-load-more">' + link_text + '</a></div>').insertAfter(paging)
										.click(function() {
											// loading.show();
											times = 1; // one more time
											win.lazyLoad('wake');
											win.lazyLoad('force');
											return false;
										});
								}
							} else {
								win.lazyLoad('wake');
							}
						} else {
							win.lazyLoad('stop');
							$('.lazyloading-load-more').hide();
						}

						/* Плиточная галерея */
						if(typeof $.pluginprotilegallery != "undefined"){
							$.pluginprotilegallery.lazyload();
						}

						// loading.hide();
						tmp.remove();
					});
				}
			});
		}
	}


});


$(function(){
    var tables = $('table.listfeatures');
    if( tables.length > 1 ) {
        var maxHeight = 0;
        tables.each(function(){
            if( $(this).height() > maxHeight ) {
                maxHeight = $(this).height();
            }
        });
        tables.each(function(){
            $(this).css('margin-bottom', maxHeight - $(this).height() + 10 );
        });
    }
});
