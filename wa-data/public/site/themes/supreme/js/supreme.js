/* supreme 1.4.8.4 */
function is_touch_device() {
	return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
}

function bootstrapForm(target) {
	$(target).find('input:not([type=checkbox],[type=radio],[type="button"],[type=submit]), select, textarea').addClass('form-control');
	$(target).find('input[type=submit], input[type=button]').addClass('btn btn-primary');
}

function MatchMedia(media_query) {
	var matchMedia = window.matchMedia,
		is_supported = (typeof matchMedia == 'function');
	if (is_supported && media_query) {
		return matchMedia(media_query).matches;
	} else {
		return false;
	}
}

function viewport() {
	var e = window,
		a = 'inner';
	if (!('innerWidth' in window)) {
		a = 'client';
		e = document.documentElement || document.body;
	}

	var wWidth = e[a + 'Width'];
	var dWidth = $(document).width();

	var wHeight = e[a + 'Height'];
	var scroollWidth = wWidth - dWidth;

	return {
		width: wWidth,
		height: wHeight,
		scrollbar: scroollWidth
	};
}

function fixScroll(action) {
	if (action == 'show') {
		var scW = viewport().scrollbar;
		if (scW > 0) {
			$('body').attr('data-scroll', scW);
			$('#fixed_toolbar').css('padding-right', scW);
			$('#f_up').css('margin-right', scW);
		}
	} else if (action == 'hide') {
		$('body').removeAttr('data-scroll');
		$('#fixed_toolbar').removeAttr('style');
		$('#f_up').removeAttr('style');
	}
}

function showItems(target) {
	$(target).next().on('click', function() {
		$(target).find('>*').show();
		$(target).next().hide();
	});
}

function stickyFooter() {
	var docHeight = $(window).height();
	var footerHeight = $('#footer').outerHeight();
	var footerTop = $('#footer').position().top + footerHeight;

	if (footerTop < docHeight) {
		$('#footer').css('margin-top', (docHeight - footerTop) + 'px');
	}
}

var BrowserDetect = {
	init: function() {
		this.browser = this.searchString(this.dataBrowser) || "Other";
		this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
	},
	searchString: function(data) {
		for (var i = 0; i < data.length; i++) {
			var dataString = data[i].string;
			this.versionSearchString = data[i].subString;

			if (dataString.indexOf(data[i].subString) !== -1) {
				return data[i].identity;
			}
		}
	},
	searchVersion: function(dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index === -1) {
			return;
		}

		var rv = dataString.indexOf("rv:");
		if (this.versionSearchString === "Trident" && rv !== -1) {
			return parseFloat(dataString.substring(rv + 3));
		} else {
			return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
		}
	},

	dataBrowser: [{
			string: navigator.userAgent,
			subString: "Edge",
			identity: "MS Edge"
		}, {
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer"
		}, {
			string: navigator.userAgent,
			subString: "Trident",
			identity: "Explorer"
		}, {
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		}, {
			string: navigator.userAgent,
			subString: "Opera",
			identity: "Opera"
		}, {
			string: navigator.userAgent,
			subString: "OPR",
			identity: "Opera"
		},
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		}, {
			string: navigator.userAgent,
			subString: "Safari",
			identity: "Safari"
		}
	]
};
BrowserDetect.init();

$.extend(true, $.magnificPopup.defaults, {
	tClose: 'Закрыть (Esc)',
	tLoading: '',
	gallery: {
		tPrev: 'Назад (Стрелка влево)',
		tNext: 'Вперед (Стрелка вправо)',
		tCounter: '%curr% / %total%'
	},
	image: {
		tError: 'Невозможно загрузить <a href="%url%">изображение</a>.'
	},
	ajax: {
		tError: 'Невозможно загрузить <a href="%url%">содержимое</a>.'
	},
	closeBtnInside: false
});

$(document).ready(function() {
	if (is_touch_device()) {
		$('html').addClass('touch');
	} else {
		$('html').addClass('no-touch');
		stickyFooter();
	}

	if(BrowserDetect.browser == 'MS Edge'){
		$('html').addClass('edge');
	}
	else{
		$('html').addClass('not-edge');
	}

	bootstrapForm('#page .ajax-form');

	$(document).on('opening', '.remodal', function(e) {
		fixScroll('show');
	});

	$(document).on('closed', '.remodal', function(e) {
		fixScroll('hide');
	});

	$('.side_block_collapsible .title').on('click', function(e) {
		$(this).next().slideToggle();
		/*
		if($.cookie('side_catalog_collapse')){
			$.removeCookie('side_catalog_collapse');
			$('.side_catalog').slideDown();
		}
		else{
			$.cookie('side_catalog_collapse', '1', { path: '/' });
			$('.side_catalog').slideUp();
		}
		*/
		return false;
	});

	$(window).on('throttledresize', function() {

		setTimeout(function() {
			stickyFooter();
		}, 200);

	});

	// MAILER app email subscribe form
	$('#mailer-subscribe-form input.charset').val(document.charset || document.characterSet);
	$('#mailer-subscribe-form').submit(function() {
		var form = $(this);

		var email_input = form.find('input[name="email"]');
		var email_submit = form.find('input[type="submit"]');
		if (!email_input.val()) {
			email_input.addClass('error');
			return false;
		} else {
			email_input.removeClass('error');
		}

		email_submit.hide();
		email_input.after('<i class="icon16 loading"></i>');

		$('#mailer-subscribe-iframe').load(function() {
			$('#mailer-subscribe-form').hide();
			$('#mailer-subscribe-iframe').hide();
			$('#mailer-subscribe-thankyou').show();
		});
	});

	// SUBSCRIBE FORMS
	$('.wa-subscribe-form').each(function() {
		$(this).parent().addClass('wa-subscribe-form-wrap');
		bootstrapForm(this);
	});

	// SIDE MENU COLLAPSIBLE
	$('.menu_collapsible .active').addClass('open').find('> .submenu').show();

	$('.menu_collapsible .sub_toggle').on('click', function() {
		$parent = $(this).closest('li');
		$parent.toggleClass('open');
		$parent.find('> .submenu').slideToggle(300);
	});


	// MENUS
	$('.menubar .menu_toggle').on('click', function() {
		$(this).parents('.menubar').find('.menu_wrapper').toggleClass('open');
	});

	// $('.menubar .sub_toggle, .menu_collapsible .sub_toggle').on('click', function() {
	$('.menubar .sub_toggle').on('click', function() {
		$(this).parent().parent().toggleClass('open');
	});

	$('.menubar .link > a[href=#]').on('click', function(e) {
		e.preventDefault();
	});

	// TOPNAV
	function topMenuInit() {
		var mWrap = $('.topnav-in');
		var m = $('.site_menu > .menu');
		var me = m.find('> li:not(.menu-more)');
		var mMore = m.find('> .menu-more');
		var mMoreMenu = mMore.find('> .submenu');
		var authMenu = $('.auth_menu');

		// clone menu to MORE
		me.clone().appendTo(mMoreMenu);
		var mme = mMoreMenu.find('>li');
		mme.addClass('hidden');

		mMore.find('> a').on('click', function(e) {
			e.preventDefault();
		});

		function menuMore(){
			me.removeClass('hidden');
			mme.addClass('hidden');

			var authMenuW = 0;
			if(authMenu.length && !MatchMedia("only screen and (min-width: 768px) and (max-width: 991px)")){
				authMenuW = authMenu.width();
			}

			var mww = mWrap.width() - authMenuW;
			var mw = m.width();

			if(mww < mw){
				mMore.removeClass('hidden');
				$(me.get().reverse()).each(function(i){
					var mww = mWrap.width() - mMore.width() - authMenuW;
					var mw = m.width() * 0.03 + m.width() - mMore.width();
					if(mww < mw){
						$(this).addClass('hidden');
						$(mme.get().reverse()).eq(i).removeClass('hidden');
					}
				});
			}
			else{
				mMore.addClass('hidden');
			}
			$('.site_menu').addClass('initialized');
		}

		if(BrowserDetect.browser == 'Safari'){
			$(window).load(function() {
				menuMore();
			});
		}
		else{
			menuMore();
		}

		$(window).on('throttledresize', function() {
			menuMore();
		});

		// Tablet top nav
		if(MatchMedia("only screen and (min-width: 768px)")){
			$(document).on('click touch', '.h_menu2 .menu > li.hassub', function () {
				if(!$(this).hasClass('hover')){
					return false;
				}
			});
		}
	}
	topMenuInit();

	// MENUBAR
	function menuInit() {
		var mWrap = $('.h_menu2 .menu_wrapper:not(.hidden-sm)');
		var m = mWrap.find('> .menu');
		var me = m.find('> li:not(.menu-more)');
		var mMore = m.find('> .menu-more');
		var mMoreMenu = mMore.find('> .submenu > ul');

		mMore.find('.link a').on('click', function(e) {
			e.preventDefault();
		});

		// clone menu to MORE
		me.clone().appendTo(mMoreMenu);
		var mme = mMoreMenu.find('>li');
		mme.addClass('hidden');

		mme.find('.submenu').removeClass('sub_all_levels').addClass('sub_one_level').parent().addClass('hassub');

		function menuMore(){

			me.removeClass('hidden');
			mme.addClass('hidden');

			var mww = mWrap.width();
			var mw = m.width();

			if(mww < mw){
				mMore.removeClass('hidden');
				$(me.get().reverse()).each(function(i){
					var mww = mWrap.width() - mMore.width();
					var mw = m.width() - mMore.width();
					if(mww < mw){
						$(this).addClass('hidden');
						$(mme.get().reverse()).eq(i).removeClass('hidden');
					}
				});
			}
			else{
				mMore.addClass('hidden');
			}
			$('.menubar').addClass('initialized');
		}

		if(BrowserDetect.browser == 'Safari'){
			$(window).load(function() {
				menuMore();
			});
		}
		else{
			menuMore();
		}

		function menuRtl() {
			if(MatchMedia("only screen and (min-width: 768px)") || (BrowserDetect.browser == 'Explorer' && BrowserDetect.version <= 9)){

				// level 1 submenu
				m.find('>li.hassub').on("mouseenter", function() {
					elLeft = $(this).offset().left;
					elWidth = $(this).width();
					subWidth = $(this).find('>.submenu').width();

					if (elLeft + subWidth > $(window).width()) {
						// sub on full width
						if (elLeft + elWidth < subWidth) {
							$(this).addClass('sub_l').find('>.submenu').css('top', $(this).position().top + $(this).height());
						}
						// sub RTL
						else {
							$(this).addClass('rtl');
						}
					}
				});

				// level > 2 submenu
				m.find('li.hassub li.hassub').on("mouseenter", function() {
					elLeft = $(this).offset().left;
					elWidth = $(this).width();
					subWidth = $(this).find('>.submenu').width();

					if (elLeft + elWidth + subWidth > $(window).width()) {
						$(this).addClass('rtl');
					}
				});
			}
		}
		menuRtl();

		$(window).on('throttledresize', function() {
			menuMore();
			menuRtl();
		});


		// MENU
		$(".side_menu_wrap .menu li, .h_menu2 .menu li").on("mouseenter", function() {
			var $this = $(this);

			var leaveTimer = $this.data("leaveTimer") || 0;
			var enterTimer = $this.data("enterTimer") || 0;
			clearTimeout(leaveTimer);
			clearTimeout(enterTimer);

			enterTimer = setTimeout(function() {
				$this.addClass("hover").siblings().removeClass("hover");
			}, 50);
			$this.data("enterTimer", enterTimer);
		});

		$(".side_menu_wrap .menu li, .h_menu2 .menu li").on("mouseleave", function() {
			var $this = $(this);
			var enterTimer = $this.data("enterTimer") || 0;
			var leaveTimer = $this.data("leaveTimer") || 0;
			clearTimeout(enterTimer);
			clearTimeout(leaveTimer);

			if(!$this.hasClass('hassub') && !$this.parents('li').hasClass('hassub')){
				$this.removeClass("hover rtl sub_l");
			}
			else{
				leaveTimer = setTimeout(function() {
					$this.removeClass("hover rtl sub_l");
				}, 300);
			}

			$this.data("leaveTimer", leaveTimer);
		});

		// open active on mobile
		$('.h_menu1 .menu_wrapper li.active, .h_menu2 .menu_wrapper li.active').addClass('open').parents('li').addClass('open');
	}
	menuInit();


	// SCROLL TO TOP
	$(document).on('scroll', function() {
		var doc = document.documentElement;
		var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

		if (top > 500) {
			$('#f_up').addClass('show');
		} else {
			$('#f_up').removeClass('show');
		}
	});

	$('#f_up').on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: 0
		}, 600);
	});


	var formRemodal = $('[data-remodal-id=remodal_ajax_form]');
	var formRemodalObj = formRemodal.remodal({
		hashTracking: false
	});

	// LOAD REMODAL AJAX FORMS
	$(document).on('click', '.h_login a, .h_reg a, .need_authorize a', function() {
		var self = $(this);

		fixScroll('show');

		formRemodal.hide();
		formRemodalObj.open();

		$.get(self.attr('href'), function(data) {
			formRemodal.find('.remodal_in').html($(data).find('#page').html());
			formRemodal.find('form').attr('action', self.attr('href'));
			bootstrapForm(formRemodal);

			setTimeout(function() {
				formRemodal.show();
			}, 150);

			if (window.grecaptcha && typeof window.onloadWaRecaptchaCallback === 'function') {
				window.onloadWaRecaptchaCallback();
			} else {
				$.getScript("https://www.google.com/recaptcha/api.js?render=explicit&onload=onloadWaRecaptchaCallback");
			}
		});

		return false;
	});

	// REMODAL - LOAD ANOTHER AJAX FORMS
	$(document).on('click', '.remodal form .wa-submit a', function() {
		var self = $(this);

		$('.remodal-wrapper').addClass('loading');

		$.get(self.attr('href'), function(data) {

			formRemodal.find('.remodal_in').html($(data).find('#page').html());
			formRemodal.find('form').attr('action', self.attr('href'));

			bootstrapForm(formRemodal);

			setTimeout(function() {
				$('.remodal-wrapper').removeClass('loading');
			}, 150);

			if (window.grecaptcha && typeof window.onloadWaRecaptchaCallback === 'function') {
				window.onloadWaRecaptchaCallback();
			} else {
				$.getScript("https://www.google.com/recaptcha/api.js?render=explicit&onload=onloadWaRecaptchaCallback");
			}
		});

		return false;
	});

	// RELOAD AJAX FORM REMODAL
	$(document).on('submit', '.remodal form', function(e) {
		e.preventDefault();

		var self = $(this);
		var form = {
			type: self.attr('method'),
			url: self.attr('action'),
			data: self.serialize()
		};

		$('.remodal-wrapper').addClass('loading');

		$.ajax({
			type: form.type,
			url: form.url,
			data: form.data,
			success: function(data, textStatus, jqXHR) {

				if ($(data).find('.ajax-form').length || $(data).find('.ajax-success').length) {
					formRemodal.find('.remodal_in').css('visibility', 'hidden').html($(data).find('#page').html());

					bootstrapForm(formRemodal);

					setTimeout(function() {
						formRemodal.find('.remodal_in').css('visibility', 'visible');
						$('.remodal-wrapper').removeClass('loading');
					}, 150);

					if (window.grecaptcha && typeof window.onloadWaRecaptchaCallback === 'function') {
						window.onloadWaRecaptchaCallback();
					} else {
						$.getScript("https://www.google.com/recaptcha/api.js?render=explicit&onload=onloadWaRecaptchaCallback");
					}

					formRemodal.find('form').attr('action', form.url);
					
					if( form.url == '/signup/' ) {
					    if( typeof yaCounter49122622 != 'undefined' ) { yaCounter49122622.reachGoal('reg1') }
					}
					
				} else {
					window.location.reload();
				}
			}
		});
	});

	// $('.menubars').after('<div class="menubar_overlay"></div>');
	//
	// $(".h_menu2").on("mouseenter", function() {
	// 	var $this = $(this);
	//
	// 	var leaveTimer = $this.data("leaveTimer") || 0;
	// 	var enterTimer = $this.data("enterTimer") || 0;
	// 	clearTimeout(leaveTimer);
	// 	clearTimeout(enterTimer);
	//
	// 	enterTimer = setTimeout(function() {
	// 		$('body').addClass('menubar_hover');
	// 	}, 50);
	// 	$this.data("enterTimer", enterTimer);
	// });
	//
	// $(".h_menu2").on("mouseleave", function() {
	// 	var $this = $(this);
	// 	var enterTimer = $this.data("enterTimer") || 0;
	// 	var leaveTimer = $this.data("leaveTimer") || 0;
	// 	clearTimeout(enterTimer);
	// 	clearTimeout(leaveTimer);
	//
	// 	leaveTimer = setTimeout(function() {
	// 		$('body').removeClass('menubar_hover');
	// 	}, 300);
	//
	// 	$this.data("leaveTimer", leaveTimer);
	// });


	// BANNER SIDE
	$('.cb_side .slider').each(function() {
		var self = $(this);
		var initial = Math.floor(Math.random() * self.find('.item').length);

		self.slick({
			initialSlide: initial,
			// fade: true,
			autoplay: true,
			autoplaySpeed: 4000,
			speed: 500,
			touchThreshold: 20,
			mobileFirst: true,
			dots: true,
			arrows: false,
			prevArrow: '<a class="slick-arrow slick-prev"><span class="fa fa-angle-left"></span></a>',
			nextArrow: '<a class="slick-arrow slick-next"><span class="fa fa-angle-right"></span></a>',
			infinite: true,
			slide: 'div.item',
			draggable: true,
			useCSS: true,
			cssEase: 'ease',
			easing: 'easeInOutCubic'
		});
	});

	// BANNER TOP
	$('.cb_top .slider').each(function() {
		var self = $(this);
		var initial = Math.floor(Math.random() * self.find('.item').length);

		self.slick({
			initialSlide: initial,
			// fade: true,
			autoplay: true,
			autoplaySpeed: 4000,
			speed: 500,
			touchThreshold: 20,
			mobileFirst: true,
			dots: true,
			arrows: true,
			prevArrow: '<a class="slick-arrow slick-prev"><span class="fa fa-angle-left"></span></a>',
			nextArrow: '<a class="slick-arrow slick-next"><span class="fa fa-angle-right"></span></a>',
			infinite: true,
			slide: 'div.item',
			draggable: true,
			useCSS: true,
			cssEase: 'ease',
			easing: 'easeInOutCubic'
		});
	});

	// magnificPopup - single image
	$('.img-popup').magnificPopup({
		type:'image',
		closeBtnInside: true,
		callbacks: {
			beforeOpen: function() {
				// just a hack that adds mfp-anim class to markup
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
				this.st.mainClass = 'mfp-zoom-in';
				$('.remodal-bg').addClass('blur');
			},
			close: function() {
				$('.remodal-bg').removeClass('blur');
			}
		}
	});

	// magnificPopup - image gallery
	$('.img-popup-gal').magnificPopup({
		type:'image',
		closeBtnInside: true,
		gallery: {
			enabled: true,
			preload: [0, 2],
		},
		callbacks: {
			beforeOpen: function() {
				// just a hack that adds mfp-anim class to markup
				this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
				this.st.mainClass = 'mfp-zoom-in';
				$('.remodal-bg').addClass('blur');
			},
			close: function() {
				$('.remodal-bg').removeClass('blur');
			}
		}
	});


});

$(function(){
    $('.h_reg').on('click', 'a', function(){
        if( typeof yaCounter49122622 != 'undefined' ) { yaCounter49122622.reachGoal('reg0') }
    });
});
